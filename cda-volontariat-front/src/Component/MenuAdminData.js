import React from 'react';

const menuAdmin = () => {

  return (
    <div>
      <div>
        <img src={require('../img/gear.jpg')} className="img-fluid rounded-circle" />
      </div>

      <div className="card-deck">
        
     
        <div className="card">
          <img src={require('../img/gear.jpg')} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">Gestion Utilisateur</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationUtilisateur">Creer Utilisateur</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerUtilisateur">Lister Utilisatuer</a>
          </nav>

        </div>
        <div className="card">
          <img src={require('../img/gear.jpg')} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">Gestion Annonce</h5>

          </div>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="CreationReservation">Creer Annonce</a>
          </nav>
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="ListerReservations">Lister Annonce</a>
          </nav>
        </div>

        

      </div>

    </div>


  )

}

export default menuAdmin;