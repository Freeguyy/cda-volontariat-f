import '@fortawesome/fontawesome-free/css/all.min.css';
import 'mdbreact/dist/css/mdb.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import LoginClient from './Pages/AccueilClient';
import Login from './Pages/Accueil';
import MenuAdmin from './Pages/MenuAdministrateur';
import MenuClient from './Pages/MenuClient';
import CreationCompte from './Pages/CreationCompte';
import './css/Accueil.css';

import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ToastProvider } from 'react-toast-notifications';
import CreationUtilisateur from "./Page/CreationUtilisateur";
import ListerUtilisateur from "./Page/ListerUtilisateur";
import ModificationUtilisateur from "./Page/ModificationUtilisateur";
import Connexion from "./Page/Connexion";
import VoirUser from "./Page/VoirUser";
import "./index.css";
import "./App.css";








class App extends Component {
 

  render() {
    
    return (
      <ToastProvider>
      <div>
       
        <Router>
      
          <Connexion />
        
        
          <Switch>
        
          
          
          <Route path="/CreationUtilisateur">
            
            <CreationUtilisateur />
          </Route>
         
          <Route path="/ListerUtilisateur">
            
            <ListerUtilisateur />
          </Route>

          <Route path="/ModificationUtilisateur/:id">
            <ModificationUtilisateur />
          </Route>

          <Route exact path="/VoirUser/:id">
            <VoirUser />
          </Route>
          <Route path="/AuthentificationAdmin">
             <Login/>
           </Route>

       <Route path="/Authentification">
           <LoginClient />
           </Route>

          <Route path="/MenuAdmin">
            <MenuAdmin/>
          </Route>

          <Route path="/MenuClient">
            <MenuClient/>
          </Route>

          <Route path="/CreationCompte">
            <CreationCompte/>
          </Route>
          
          
          <Route path="/">
            
          </Route>
        </Switch>
        </Router>        
      </div>
      </ToastProvider>
    );
  }
}

export default App;
