
import React, { useState} from 'react';
import {useHistory, Link} from 'react-router-dom';
import {MDBBtn} from 'mdbreact';
import '../css/Accueil.css'


export default function Login(props) {
  

  return(
    <form>

<br></br>
<br></br>
<br></br>
<br></br>
      <div className="classic-form-page">
   <div className="container">
    <div className="row">
      <div className="col-md-4 mb-4 mx-auto">
  <div className="card">
    <div className="card-body">
      <p className="h5 text-center mb-4">CONNEXION </p>
      <br/>
      <div className="md-form">
    <i className="fa fa-user prefix  ">  </i> 
    <input type="text" id="identifiant" className="form-control light-text" ></input>
      <label className="active" >Identifiant</label>
      </div>
      <br/>
      <div className="md-form">
        <i className="fa fa-lock prefix  "> </i>
        <input type="password" id="mdp" className="form-control light-text" ></input>
        <label className="active" >Mot de passe</label>
      </div>
      <br/>
      <div className="text-center mt-4">
      <input className="btn btn-danger" type="reset" value="Annuler"></input>
      <MDBBtn color="success" href="/MenuAdmin" type="button" >Valider</MDBBtn>
      </div>
      <div className="text-center">
                      <Link className="small" to="/Authentification">Connexion en tant que client</Link>
                        
                      </div>
                      <div className="text-center">
                      <Link className="small" to="/CreationCompte">Pas encore membre?</Link>
                        
                      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div>

 
  </form>

  );

}
     
