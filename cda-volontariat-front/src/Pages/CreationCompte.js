import React from 'react';
import Header from '../Composants/Header';
import FormCreationCompte from '../Composants/FormCreationCompte';

export default function CreationCompte(){

    return(
        <div>
        <Header/>
        
        <FormCreationCompte/>
        </div>
    );

}