import React, { useState } from 'react';
import { MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon, 
MDBDropdown, MDBDropdownToggle, MDBDropdownItem, MDBDropdownMenu, MDBFormInline, MDBBtn 
, MDBInput} from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';



  export default function Header() {
    const [collapse, setCollapse] = useState(false);
    const [wideEnough, setWideEnough] = useState(false);



    function onClick() {
        setCollapse(!collapse);
    }

    function deconnexionOnClick() {
      sessionStorage.clear();
  }

    const bgBlue = {backgroundColor: '#3364FF'}
    const container = {height: 1300}
    return(
      <div>
        <Router>
          <header>
            <MDBNavbar style={bgBlue} dark expand="md" scrolling fixed="top">
              <MDBNavbarBrand href="/">
                  <strong>MENU</strong>
              </MDBNavbarBrand>
              <MDBNavbarToggler  />
              <MDBCollapse  navbar>
                <MDBNavbarNav left>
                <MDBNavItem active>
                      <MDBBtn color='#3364FF' className='white-text' href="/MenuClient">Accueil</MDBBtn>
                  </MDBNavItem>
                </MDBNavbarNav>
              </MDBCollapse>
            </MDBNavbar>
          </header>
        </Router>
      </div>
    );
  }