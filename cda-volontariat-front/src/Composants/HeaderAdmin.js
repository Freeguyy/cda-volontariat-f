import React, { useState } from 'react';
import { MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon, 
MDBDropdown, MDBDropdownToggle, MDBDropdownItem, MDBDropdownMenu, MDBFormInline, MDBBtn 
, MDBInput} from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';



  export default function HeaderAdmin() {
    const [collapse, setCollapse] = useState(false);
    const [wideEnough, setWideEnough] = useState(false);



    function onClick() {
        setCollapse(!collapse);
    }

    function deconnexionOnClick() {
      sessionStorage.clear();
  }

    const bgBlue = {backgroundColor: '#3364FF'}
    const container = {height: 1300}
    return(
      <div>
        <Router>
          <header>
            <MDBNavbar style={bgBlue} dark expand="md" scrolling fixed="top">
              <MDBNavbarBrand href="/">
                  <strong>MENU Administrateur</strong>
              </MDBNavbarBrand>
              <MDBNavbarToggler  />
              <MDBCollapse  navbar>
                <MDBNavbarNav left>
                  <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <div className="d-none d-md-inline">Gestion</div>
                  </MDBDropdownToggle>
                  <MDBDropdownMenu>
                    <MDBDropdownItem href="#!">Gestion utilisateurs</MDBDropdownItem>
                    <MDBDropdownItem href="#!">Gestion annonces</MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
                  <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <div className="d-none d-md-inline">Annonces</div>
                  </MDBDropdownToggle>
                  <MDBDropdownMenu>
                    <MDBDropdownItem href="#!">Poster une annonce</MDBDropdownItem>
                    <MDBDropdownItem href="#!">Afficher les annonces</MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
                </MDBNavbarNav>
                <MDBNavbarNav left>
                <MDBFormInline className="md-form mr-auto m-0">
                <MDBInput className="form-control mr-sm-2"  type="text" placeholder="Search" aria-label="Search" />
                <MDBBtn outline color="white" size="sm" type="submit" className="mr-auto">
                  Search
                </MDBBtn>
              </MDBFormInline>
                </MDBNavbarNav>
                <MDBNavbarNav right>
                  <MDBNavItem >
                    <MDBBtn color='#3364FF' href="/Authentification"><MDBIcon style={{color:'red'}}  fas icon ="power-off"/></MDBBtn>
                  </MDBNavItem>
    
                </MDBNavbarNav>
              </MDBCollapse>
            </MDBNavbar>
          </header>
        </Router>
      </div>
    );
  }

