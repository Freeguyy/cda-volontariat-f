import React, { useState, useEffect } from 'react';
import { MDBRow, MDBCol, MDBBtn, MDBInput } from "mdbreact";
import ModalUser from './ModalUser';
import { useHistory } from 'react-router-dom';

export default function FormCreationCompte() {
    const [user, setUser] = useState({
      "id": 0,
      "nom": "",
      "prenom": "",
      "mail": "",
      "tel": "",
      "adresse": "",
      "dateDeNaissance": "",
      "role": {
        "id": 0,
        "libelle": ""
      },
      "fonction": {
        "id": 0,
        "libelle": ""
      },
      "authentification": {
        "login": "",
        "mdp": ""
      },
      "actif": true
    });
    const [fonctions, setFonctions] = useState([]);
    const [roles, setRoles] = useState([]);
    const [reponseServeur, setReponseServeur] = useState({});
    const [show, setShow] = useState(false)
    const history = useHistory();
  
   
  
  
    const getFonctions = async () => {
      let response = await fetch(`${process.env.REACT_APP_API_URL}/fonction`, {
        headers: {
          'Authorization': sessionStorage.token
        }
      }).catch(error => console.log(error))
      
    }
  
    const getRoles = async () => {
      let response = await fetch(`${process.env.REACT_APP_API_URL}/role`, {
        headers: {
          'Authorization': sessionStorage.token
        }
      })
        .catch(error => console.log(error))
      
    }
  
    function changeHandler(event) {
      let userTemp = user
      if (event.target.name === "fonction") {
        userTemp.fonction = fonctions.find(e => e.id == event.target.value)
      } else if (event.target.name === "role") {
        userTemp.role = roles.find(e => e.id == event.target.value)
      } else if (event.target.name === "mdp") {
        userTemp.authentification.mdp = event.target.value
      } else if (event.target.name === "login") {
        userTemp.authentification.login = event.target.value
      } else {
        userTemp[event.target.name] = event.target.value;
      }
      setUser(userTemp)
    };
  
  
  
    async function submitForm(event) {
      event.preventDefault();
      let response = await fetch(`${process.env.REACT_APP_API_URL}/personne`, {
        method: "POST",
  
        body: JSON.stringify(user),
  
        headers: {
          'Accept': 'application/json',
          "Content-type": "application/json; charset=UTF-8",
          'Authorization': sessionStorage.token
        }
      })
  
    };
  
    useEffect(() => {
      getFonctions();
      getRoles();
  
    }, []);
  
  
  
    const fonctionsList = fonctions.map((fonction) => <option key={fonction.id} value={fonction.id} >{fonction.libelle}</option>);
    const rolesList = roles.map((role) => <option key={role.id} value={role.id} >{role.libelle}</option>);
  
  
  
    return <div className="user">
       
      <h3>Formulaire nouveau membre : </h3>
      <hr />
      <form
        className="needs-validation formUser"
        onSubmit={submitForm}
        noValidate
      >
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.nom}
              name="nom"
              onChange={changeHandler}
              type="text"
              id="nom"
              label="Nom"
              required
            >
              <em className="text-center red-text" >{reponseServeur.nomKO}</em>
            </MDBInput>
  
          </MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.prenom}
              name="prenom"
              onChange={changeHandler}
              type="text"
              id="prenom"
              label="Prenom"
              required >
              <em className="text-center red-text" >{reponseServeur.prenomKO}</em>
            </MDBInput>
  
          </MDBCol>
        </MDBRow>
  
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.mail}
              name="mail"
              onChange={changeHandler}
              type="text"
              id="mail"
              label="Email"
              required>
              <em className="text-center red-text" >{reponseServeur.mailKO}</em>
            </MDBInput>
          </MDBCol >
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.tel}
              name="tel"
              onChange={changeHandler}
              type="tel"
              id="tel"
              label="Telephone"
              required >
              <em className="text-center red-text" >{reponseServeur.telKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
  
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="8" className="mb-6">
            <MDBInput
              valueDefault={user.dateDeNaissance}
              name="dateDeNaissance"
              onChange={changeHandler}
              type="date"
              id="adresse"
              label="Date de naissance"
              required>
              <em className="text-center red-text" >{reponseServeur.dateKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="8" className="mb-6">
            <MDBInput
              valueDefault={user.adresse}
              name="adresse"
              onChange={changeHandler}
              type="text"
              id="adresse"
              label="Adresse"
              required>
              <em className="text-center red-text" >{reponseServeur.adresseKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center" >
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.authentification.login}
              name="login"
              onChange={changeHandler}
              type="text"
              id="login"
              label="Login"
              required
            >
              <em className="text-center red-text" >{reponseServeur.loginKO}</em>
            </MDBInput>
          </MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.authentification.mdp}
              name="mdp"
              onChange={changeHandler}
              type="password"
              id="mdp"
              label="Mot de passe"
              required
            >
              <em className="text-center red-text" >{reponseServeur.mdpKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3" ></MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBBtn color="success" type="button" >Valider</MDBBtn>
          </MDBCol>
        </MDBRow>
      </form>
      <ModalUser user={reponseServeur} show={show} setShow={setShow} />
    </div>
  }