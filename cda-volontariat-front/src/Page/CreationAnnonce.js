import React, {useState, useEffect, Fragment } from "react";


export default function CreationReservation() {

  const [client, setClients] = useState([]);
  const [libelle, setLibelle] = useState('');
  const [description, setDescription]= useState('');
  const [type, setType] = useState('');
  const [annonce, setAnnonce] = useState('');
  const [date, setDate] = useState([]);
  const [idClient, setIdClient] = useState(0);
  const [isBusy, setBusy] = useState(true);


  const getClients = () => {
    fetch(`${process.env.REACT_APP_URL}/volontaire`, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        setClients(data);
        setIdClient(data[0].id);
        
        setBusy(false);
      })
      .catch(error => {
        console.log(error);
      });
  }

  useEffect(() => {
    const fetchData = async () => {
      await getClients();
    }
    fetchData()
  }, []);



  const changeHandlerLibelle = e => {
    setLibelle(e.target.value);
  }
  const changeHandlerDescription = e => {
    setDescription(e.target.value);
  }
  const changeHandlerType = e => {
    setType(e.target.value);
  }

  const changeHandlerAnnonce = e => {
    setAnnonce(e.target.value);
  };

  // à chaque modification du champ, il set les données du champ
  const changeHandlerDate = e => {
    setDate(e.target.value);
  };

  const changeHandlerIDClient = e => {
    setIdClient(e.target.value);

  };



  





  function handleSubmit(e) {
    //il permet de ne pas recharger la page avant d'envoyer les données
    e.preventDefault();

    if(compare()){
        //pour envoyer les données 
    fetch(`${process.env.REACT_APP_URL}/annonces`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json; charset=UTF-8'
      },
      // .stringgify pour transformer en string
      body: JSON.stringify({
        libelle: libelle,
        date: date,
        description: description,
        type:type,

       
        personne : {
          type: "client",
          id: idClient
        },
        
      })
    }).then(function (response) {
     
      if (response.status === 200) {
       alert('ok');
        window.location.href = "/ListerReservations";
      }
    });
    }else{
      alert('ko');
    }
    
    
  }
  return <Fragment>

    {isBusy ? (<div></div>) : (
      <div>
        <form >

          <div className="form-group col-md-6">
            <label htmlFor="nom">Annonce</label>
            <input type="text" className="form-control" id="nom" placeholder="nom de l'annonce" name="nom" value={annonce} onChange={changeHandlerAnnonce} required />
          </div>


          <div className="form-group col-md-6">
            <label htmlFor="startDate">Date :</label>
            <input type="date" className="form-control" id="Date" name="date" value={date} onChange={changeHandlerDate} required />
          </div>

          <div className="form-group col-md-6">
            <label htmlFor="selectPersonne">Sélectionner le formateur qui souhaite réserver :</label> 
            <select className="form-control" id="selectPersonne" name="selectPersonne" value={idClient} onChange={changeHandlerIDClient}>
            {clients.map((client) =>
           
              <option key = {client.id} value={client.id}>{client.nom}</option>
              )}
            </select>
          </div>

          <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Valider</button>
        </form>
      </div>
    )}
  </Fragment>;


}

