import React, { useState } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';

function FormPage (){

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [isBusy, setBusy] = useState(true);

  


return (
    <div class="bg">
<MDBContainer>
  <MDBRow>
    <MDBCol md="6" className="formRole">
      <form>
        <p className="h5 text-center mb-4"></p>
       
        <div className="text-center">
          <MDBBtn href="/MenuAdmin">Se connecter</MDBBtn>
        </div>
      </form>
      <div className="grey-text">
          <MDBBtn href="/ChoixRole" label="pas encore membre ?" name="inscription">Je sais pas</MDBBtn>
        </div>
    </MDBCol>
  </MDBRow>
</MDBContainer>
</div>
);
};

export default FormPage;